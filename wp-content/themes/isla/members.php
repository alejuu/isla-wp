<?php
/*
 Template Name: members
 */
?>
<?php
get_header( 'smallheader' ); ?>

<?php if ( have_posts() ) : ?>
    <div> 
        <?php while ( have_posts() ) : the_post(); ?>
            <article> 
                <div class="container-fluid container-members-1 container-center-full"> 
                    <div class="container"> 
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h1 class="text-uppercase white"><?php _e( 'Become an ISLA member', 'isla' ); ?></h1>
                                <a href="<?php the_field('become_link'); ?>"> 
                                    <button type="button" class="btn btn-primary">
                                        <?php the_field('become_btn'); ?>
                                    </button>                                                     
                                </a>                                                 
                            </div>
                        </div>                                         
                    </div>                                     
                </div>                                 
                <div class="container-fluid container-members-2 blue-bg"> 
                    <div class="row">
                        <div class="container"> 
                            <div class="row ch2-bounceInUp invisible"> 
                                <div class="col-md-12">
                                    <?php the_content(); ?>
                                </div>                                                 
                            </div>                                             
                        </div>
                    </div>                                     
                </div>                                 
                <div class="container-fluid container-home-3 gray-lighter-bg"> 
                    <div class="row">
                        <div class="container"> 
                            <div class="row ch3-bounceInUp invisible"> 
                                <div class="col-md-12 col-margin-down ch3-col text-center"> 
                                    <h2><?php _e( 'Join Us!', 'isla' ); ?></h2> 
                                    <h3><?php _e( 'Take the first step by becoming an ISLA Member', 'isla' ); ?></h3> 
                                </div>                                                 
                                <div class="col-md-4 col-members text-center col-sm-4"> 
                                    <div class="pricing-table text-center"> 
                                        <ul> 
                                            <li class="pricing-table-header black-bg text-uppercase">
                                                <?php _e( 'Bronze', 'isla' ); ?>
                                            </li>                                                             
                                            <li class="pricing-table-price gray-darker-bg"> 
                                                <sup><?php _e( '$', 'isla' ); ?></sup> 
                                                <span><?php the_field('bnz_price'); ?></span> 
                                                <sub class="text-uppercase"><?php _e( '/year', 'isla' ); ?></sub> 
                                            </li>                                                             
                                            <?php if( have_rows('bnz_li') ): ?> 
                                                <?php while( have_rows('bnz_li') ): the_row(); ?> 
                                                    <li> 
                                                        <span><?php the_sub_field('bnz_inc'); ?></span> 
                                                    </li>                                                                     
                                                <?php endwhile; ?> 
                                            <?php endif; ?> 
                                            <li class="pricing-table-footer blue-bg text-uppercase"> 
                                                <a href="<?php the_sub_field('bnz_link'); ?>"><?php _e( 'Become a bronze member', 'isla' ); ?></a> 
                                            </li>                                                             
                                        </ul>                                                         
                                    </div>                                                     
                                </div>                                                 
                                <div class="col-md-4 col-members text-center col-sm-4"> 
                                    <div class="pricing-table text-center"> 
                                        <ul> 
                                            <li class="pricing-table-header black-bg text-uppercase">
                                                <?php _e( 'Silver', 'isla' ); ?>
                                            </li>                                                             
                                            <li class="pricing-table-price gray-darker-bg"> 
                                                <sup><?php _e( '$', 'isla' ); ?></sup> 
                                                <span><?php the_field('slv_price'); ?></span> 
                                                <sub class="text-uppercase"><?php _e( '/year', 'isla' ); ?></sub> 
                                            </li>                                                             
                                            <?php if( have_rows('slv_li') ): ?> 
                                                <?php while( have_rows('slv_li') ): the_row(); ?> 
                                                    <li> 
                                                        <span><?php the_sub_field('slv_inc'); ?></span> 
                                                    </li>                                                                     
                                                <?php endwhile; ?> 
                                            <?php endif; ?> 
                                            <li class="pricing-table-footer blue-bg text-uppercase"> 
                                                <a href="<?php the_sub_field('slv_link'); ?>"><?php _e( 'Become a silver member', 'isla' ); ?></a> 
                                            </li>                                                             
                                        </ul>                                                         
                                    </div>                                                     
                                </div>                                                 
                                <div class="col-md-4 col-members text-center col-sm-4"> 
                                    <div class="pricing-table text-center"> 
                                        <ul> 
                                            <li class="pricing-table-header black-bg text-uppercase">
                                                <?php _e( 'Gold', 'isla' ); ?>
                                            </li>                                                             
                                            <li class="pricing-table-price gray-darker-bg"> 
                                                <sup><?php _e( '$', 'isla' ); ?></sup> 
                                                <span><?php the_field('gld_price'); ?></span> 
                                                <sub class="text-uppercase"><?php _e( '/year', 'isla' ); ?></sub> 
                                            </li>                                                             
                                            <?php if( have_rows('gld_li') ): ?> 
                                                <?php while( have_rows('gld_li') ): the_row(); ?> 
                                                    <li> 
                                                        <span><?php the_sub_field('gld_inc'); ?></span> 
                                                    </li>                                                                     
                                                <?php endwhile; ?> 
                                            <?php endif; ?> 
                                            <li class="pricing-table-footer blue-bg text-uppercase"> 
                                                <a href="<?php the_sub_field('gld_link'); ?>"><?php _e( 'Become a gold member', 'isla' ); ?></a> 
                                            </li>                                                             
                                        </ul>                                                         
                                    </div>                                                     
                                </div>                                                 
                            </div>                                             
                        </div>
                    </div>                                     
                </div>                                 
                <div class="container-fluid container-home-3"> 
                    <div class="row">
                        <div class="container"> 
                            <div class="row ch3-bounceInUp invisible"> 
                                <div class="col-md-12 col-margin-down ch3-col text-center"> 
                                    <h2><?php _e( 'Why Become A Member!', 'isla' ); ?></h2> 
                                </div>                                                 
                                <div class="col-md-4 col-members text-center col-sm-4"> 
                                    <a href="#" class="thumbnail circle img-members"> 
                                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-members-1.png" alt="" width="100%"> 
                                    </a>                                                     
                                    <p><?php _e( 'ISLA Members save 30 to 60% lorem ipsum', 'isla' ); ?></p> 
                                </div>                                                 
                                <div class="col-md-4 col-members text-center col-sm-4"> 
                                    <a href="#" class="thumbnail circle img-members"> 
                                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-members-2.png" alt="" width="100%"> 
                                    </a>                                                     
                                    <p><?php _e( 'Order products directly from affiliate companies', 'isla' ); ?></p> 
                                </div>                                                 
                                <div class="col-md-4 col-members text-center col-sm-4"> 
                                    <a href="#" class="thumbnail circle img-members"> 
                                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-members-3.png" alt="" width="100%"> 
                                    </a>                                                     
                                    <p><?php _e( 'Prodeal coupon code sent in membership email', 'isla' ); ?></p> 
                                </div>                                                 
                                <div class="col-md-12 col-margin-up text-center"> 
                                    <p><?php _e( '...plus prodeals on over 300 brands!', 'isla' ); ?></p> 
                                </div>                                                 
                                <?php if( have_rows('member_brands') ): ?> 
                                    <?php while( have_rows('member_brands') ): the_row(); ?> 
                                    <div class="col-md-3 text-center membership-brands col-sm-3 col-xs-6"> 
                                            <?php $image = get_sub_field('brand_logo');  if( !empty($image) ): ?> 
                                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" width="100%" /> 
                                        <?php endif; ?> 
                                        </div>                                                         
                                    <?php endwhile; ?> 
                                <?php endif; ?> 
                            </div>                                             
                        </div>
                    </div>                                     
                </div>                                 
            </article>
        <?php endwhile; ?> 
    </div>
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.', 'isla' ); ?></p>
<?php endif; ?> 
<!-- Conteiner 1 - Image : Start -->                 
<!-- Conteiner 1 - Image : End -->                 
<!-- Conteiner 2 - Blue - Learn More About Us : Start -->                 
<!-- Conteiner 2 - Blue - Learn More About Us : End -->                 
<!-- Conteiner 3 - Gray Lighter - Tables : Start -->                 
<!-- Conteiner 3 - Gray Lighter - Tables : End -->                 
<!-- Conteiner 4 - White - Prodeals : Start -->                 
<!-- Conteiner 4 - White - Prodeals : End -->                                 

<?php get_footer( 'smallheader' ); ?>