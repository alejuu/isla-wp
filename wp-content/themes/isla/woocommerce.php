<?php
get_header( 'smallheader' ); ?>

<section class="top-margin"> 
    <div class="container-fluid"> 
        <div class="row"> 
            <div class="col-sm-3">
                <?php if ( is_active_sidebar( 'store_sidebar' ) ) : ?>
                    <div id="main_sidebar">
                        <?php dynamic_sidebar( 'store_sidebar' ); ?>
                    </div>
                <?php endif; ?>                                  
            </div>                             
            <div class="col-sm-9 col-md-9 col-store-content"> 
                <div>
                    <?php woocommerce_content() ?>
                </div>                                 
            </div>                             
        </div>                         
    </div>                     
</section>                                 

<?php get_footer( 'smallheader' ); ?>