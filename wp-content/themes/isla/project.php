<?php
/*
 Template Name: project
 */
?>
<?php
get_header( 'smallheader' ); ?>

                <?php if ( have_posts() ) : ?>
                    <div> 
                        <?php while ( have_posts() ) : the_post(); ?>
                        <article> 
                                <?php $image = get_field('project_intro_img');  if( !empty($image) ): ?> 
                                <div class="container-fluid container-project-1 container-center-full" style="background-image:url('<?php echo $image['url']; ?>');display:<?php echo get_post_meta( get_the_ID(), 'header_display', true ); ?>;">
                                    <h1 class="white text-uppercase"><?php the_field('intro_title'); ?></h1>
                                </div>                                 
                            <?php endif; ?> 
                            <div class="container container-project-2" style="display:<?php echo get_post_meta( get_the_ID(), 'intro_display', true ); ?>;">
                                <div class="row ch2-bounceInUp invisible">
                                    <?php the_field('intro_content'); ?>
                                </div>
                            </div>                             
                            <div class="container-fluid container-project-3 blue-bg no-padding hidden-sm hidden-xs" style="display:<?php echo get_post_meta( get_the_ID(), 'squares_display', true ); ?>;"> 
                            <div class="row eq-height ch3-bounceInUp invisible">
                                    <div class="col-md-4 blue-extra-bg text-center white no-margin padding-60-20 vertical-middle">
                                        <?php the_field('square_text_one'); ?>
                                    </div>
                                    <div class="col-md-4 blue-bg text-center no-margin white padding-60-20 vertical-middle">
                                        <?php the_field('square_text_two'); ?>
                                    </div>
                                    <?php $image = get_field('square_img_one');  if( !empty($image) ): ?>
                                    <div class="col-md-4 img-square-project" style="background-image:url('<?php echo $image[url]; ?>');">
</div>
                                <?php endif; ?>
                                </div>                                 
                            <div class="row eq-height ch3-bounceInUp invisible">
                                    <div class="col-md-4 blue-bg text-center no-margin white padding-60-20 vertical-middle">
                                        <?php the_field('square_text_three'); ?>
                                    </div>
                                    <?php $image = get_field('square_img_two');  if( !empty($image) ): ?>
                                    <div class="col-md-4 img-square-project" style="background-image:url('<?php echo $image[url]; ?>');">
</div>
                                <?php endif; ?>
                                <div class="col-md-4 blue-darker-bg text-center no-margin white padding-60-20 vertical-middle">
                                    <?php the_field('square_text_four'); ?>
                                </div>
                                </div>                                 
                            <div class="row eq-height ch3-bounceInUp invisible">
                                    <?php $image = get_field('square_img_three');  if( !empty($image) ): ?>
                                    <div class="col-md-4 img-square-project" style="background-image:url('<?php echo $image[url]; ?>');">
</div>
                                <?php endif; ?>
                                <div class="col-md-4 blue-darker-bg text-center no-margin white padding-60-20 vertical-middle">
                                    <?php the_field('square_text_five'); ?>
                                </div>
                                <div class="col-md-4 blue-extra-bg text-center no-margin white padding-60-20 vertical-middle">
                                    <?php the_field('square_text_six'); ?>
                                </div>
                                </div>                                 
                            </div>                             
                            <div class="container-fluid container-project-4"> 
                                <div class="container"> 
                                    <div class="row ch4-bounceInUp invisible"> 
                                        <div style="display:<?php echo get_post_meta( get_the_ID(), 'details_display', true ); ?>;"> 
                                            <div class="col-md-12 col-margin-down text-center">
                                                <h2><?php the_title(); ?></h2>
                                                <h5 class="text-uppercase"><?php the_field('project_date'); ?></h5>
                                                <div class="center-divider"></div>
                                            </div>                                             
                                            <div class="col-md-6 col-sm-6">
                                                <div class="circle-badge" style="visibility:<?php echo get_post_meta( get_the_ID(), 'badge', true ); ?>;">
                                                    <p><?php the_field('project_status'); ?></p>
                                                </div>
                                                <?php $image_attributes = (is_singular() || in_the_loop()) ? wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' ) : null; ?>
                                                <div class="img-category-page" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>"></div>
                                            </div>                                             
                                            <div class="col-md-6 col-sm-6">
                                                <h5 class="text-uppercase gray-light"><?php _e( 'Location', 'isla' ); ?></h5>
                                                <p><?php the_field('project_location'); ?></p>
                                                <h5 class="text-uppercase gray-light"><?php _e( 'Status', 'isla' ); ?></h5>
                                                <p class="category-status-desc no-margin"><?php the_field('project_status_desc'); ?></p>
                                                <p class="no-margin"><?php the_field('project_app_due'); ?></p>
                                                <p><?php the_field('project_volunteers'); ?></p>
                                            </div>                                             
                                        </div>                                         
                                        <div style="display:<?php echo get_post_meta( get_the_ID(), 'content_display', true ); ?>;">
                                            <div class="col-md-12 col-sm-12 col-margin-up">
                                                <?php the_content(); ?>
                                            </div>
                                        </div>                                         
                                        <div class="col-md-12 col-sm-12 col-margin-up" style="display:<?php echo get_post_meta( get_the_ID(), 'dates_display', true ); ?>;"> 
                                            <h3 class="blue"><?php _e( 'Important Dates and Project Itinerary', 'isla' ); ?></h3> 
                                            <div> 
                                                <?php if( have_rows('project_iti') ): ?> 
                                                    <div> 
                                                        <?php while( have_rows('project_iti') ): the_row(); ?> 
                                                            <div>
                                                                <?php if( have_rows('project_iti_date') ): ?>
                                                                    <div>
                                                                        <?php while( have_rows('project_iti_date') ): the_row(); ?>
                                                                            <p><strong><?php the_sub_field('new_date'); ?></strong></p>
                                                                            <p><?php the_sub_field('date_desc'); ?></p>
                                                                        <?php endwhile; ?>
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>                                                             
                                                            <div class="divider"></div>                                                             
                                                        <?php endwhile; ?> 
                                                    </div>                                                     
                                                <?php endif; ?> 
                                            </div>                                             
                                        </div>                                         
                                        <div class="col-md-12 col-sm-12 col-margin-up" style="display:<?php echo get_post_meta( get_the_ID(), 'cost_display', true ); ?>;">
                                            <h3 class="blue"><?php _e( 'Cost', 'isla' ); ?></h3>
                                            <strong><?php _e( 'ISLA Project Donation:', 'isla' ); ?></strong>
                                            <p><?php the_field('project_donation'); ?></p>
                                            <strong><?php _e( 'Included in the ISLA Project Donation:', 'isla' ); ?></strong>
                                            <?php the_field('project_donation_included'); ?>
                                            <strong><?php _e( 'NOT Included in the ISLA Project Donation:', 'isla' ); ?></strong>
                                            <?php the_field('project_donation_not_included'); ?>
                                        </div>                                         
                                        <div class="col-md-12 col-sm-12 col-margin-up" style="display:<?php echo get_post_meta( get_the_ID(), 'lang_display', true ); ?>;">
                                            <h3 class="blue"><?php _e( 'Language Requirement', 'isla' ); ?></h3>
                                            <p><?php the_field('project_lang'); ?></p>
                                        </div>                                         
                                    </div>                                     
                                </div>                                 
                            </div>                             
                            <div class="container-fluid container-project-5 blue-bg" style="display:<?php echo get_post_meta( get_the_ID(), 'apply_display', true ); ?>;"> 
                                <div class="row ch5-bounceInUp invisible"> 
                                    <div class="col-md-12 col-margin-down text-center">
                                        <h2 class="white"><?php the_field('apply_title'); ?></h2>
                                        <p class="white"><?php the_field('apply_text'); ?></p>
                                    </div>                                     
                                    <div class="col-md-12 col-margin-up text-center">
                                        <a href="<?php the_field('apply_link'); ?>">
                                            <button type="button" class="btn btn-primary">
                                                <?php the_field('apply_btn'); ?>
                                            </button>
                                        </a>
                                    </div>                                     
                                </div>                                 
                            </div>                             
                            <div class="container-fluid container-project-6 gray-dark-bg" style="display:<?php echo get_post_meta( get_the_ID(), 'vol_display', true ); ?>;"> 
                                <div class="container"> 
                                    <div class="row ch6-bounceInUp invisible"> 
                                        <div class="col-md-12 col-margin-down text-center">
                                            <h2 class="white"><?php the_field('vol_title'); ?></h2>
                                        </div>                                         
                                        <div class="col-md-12 volunteers text-center"> 
                                            <?php if( have_rows('volunteers') ): ?> 
                                                <div> 
                                                    <?php while( have_rows('volunteers') ): the_row(); ?> 
                                                        <div class="volunteer">
                                                        <a href="<?php the_sub_field('vol_link'); ?>">
                                                                <?php $image = get_sub_field('vol_img');  if( !empty($image) ): ?>
                                                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" width="100%" />
                                                            <?php endif; ?>
                                                            </a>
                                                            <a href="<?php the_sub_field('vol_link'); ?>"><?php the_sub_field('name'); ?></a>
                                                        </div>                                                         
                                                    <?php endwhile; ?> 
                                                </div>                                                 
                                            <?php endif; ?> 
                                        </div>                                         
                                    </div>                                     
                                </div>                                 
                            </div>                             
                            <?php if( have_rows('past_projects') ): ?> 
                                <div class="past-projects"> 
                                    <?php while( have_rows('past_projects') ): the_row(); ?> 
                                        <div class="container-fluid past-project"> 
                                            <div class="container no-padding"> 
                                                <div class="row ch5-bounceInUp invisible"> 
                                                    <div class="col-md-12 col-margin-down text-center">
                                                        <h2><?php the_sub_field('past_project_title'); ?></h2>
                                                        <h5 class="text-uppercase"><?php the_sub_field('past_project_date'); ?></h5>
                                                        <div class="center-divider"></div>
                                                    </div>                                                     
                                                    <div class="col-md-12 col-margin-down">
                                                        <?php the_sub_field('past_project_content'); ?>
                                                    </div>                                                     
                                                    <?php if( have_rows('single_past_project') ): ?> 
                                                        <div class="single-past-project"> 
                                                            <?php while( have_rows('single_past_project') ): the_row(); ?> 
                                                                <div class="col-md-6 col-sm-6"> 
                                                                    <div class="col-md-5 col-sm-12">
                                                                    <a href="<?php the_sub_field('single_past_project_link'); ?>">
                                                                            <?php $image = get_sub_field('single_past_project_img');  if( !empty($image) ): ?>
                                                                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" width="100%" />
                                                                        <?php endif; ?>
                                                                        </a>
                                                                    </div>                                                                     
                                                                    <div class="col-md-7 col-sm-12">
                                                                        <h6><?php the_sub_field('single_past_project_date'); ?></h6>
                                                                        <a href="<?php the_sub_field('single_past_project_link'); ?>" class="h4 text-uppercase"><?php the_sub_field('single_past_project_title'); ?></a>
                                                                        <div class="divider"></div>
                                                                        <p><?php the_sub_field('single_past_project_content'); ?></p>
                                                                        <a href="<?php the_sub_field('single_past_project_link'); ?>"><h4 class="blue"><?php _e( 'Project Details >', 'isla' ); ?></h4></a>
                                                                    </div>                                                                     
                                                                </div>                                                                 
                                                            <?php endwhile; ?> 
                                                        </div>                                                         
                                                    <?php endif; ?> 
                                                </div>                                                 
                                            </div>                                             
                                        </div>                                         
                                    <?php endwhile; ?> 
                                </div>                                 
                            <?php endif; ?> 
                            </article>
                        <?php endwhile; ?> 
                    </div>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'isla' ); ?></p>
                <?php endif; ?> 
                <!-- Conteiner 1 - Full Bg Image : Start -->                 
                <!-- Conteiner 1 - Full Bg Image : End -->                 
                <!-- Conteiner 2 - White - FPO : Start -->                 
                <!-- Conteiner 2 - White - FPO : End -->                 
                <!-- Conteiner 3 - Blue - Squres : Start -->                 
                <!-- Conteiner 3 - Blue - Members : End -->                 
                <!-- Conteiner 4 - White - Project content : Start -->                 
                <!-- Conteiner 4 - White - Project content : End -->                 
                <!-- Conteiner 5 - Blue - Why : Start -->                 
                <!-- Conteiner 5 - Blue - Why : End -->                 
                <!-- Conteiner 6 - Gray Dark - Volunteers : Start -->                 
                <!-- Conteiner 6 - Gray Dark - Volunteers : End -->                 
                <!-- Conteiner 7 - Gray Lighter - News : Start -->                 
                <!-- Conteiner 7 - Gray Lighter - News : End -->                 
                <!-- Conteiner 8 - White - News : Start -->                 
                <!-- Conteiner 8 - White - News : End -->                 
                <!-- Conteiner 9 - Gray Lighter - News : Start -->                 
                <!-- Conteiner 9 - Gray Lighter - News : End -->                 
                <!-- Conteiner 10 - White - News : Start -->                 
                <!-- Conteiner 10 - White - News : End -->                 
                <!-- Conteiner 11 - Gray Lighter - News : Start -->                 
                <!-- Conteiner 11 - Gray Lighter - News : End -->                 
                <!-- Conteiner 7 - Background Image : Start -->                 
                <div class="container-fluid container-home-7"> 
                    <div class="row ch7-bounceInUp invisible">
                        <div class="col-md-12 text-center">
                            <h2 class="white"><?php _e( 'Interested in traveling the globe as a humanitarian lifeguard?', 'isla' ); ?></h2>
                            <h5 class="white"><?php _e( 'Congratulations, you’re at the first step to your ISLA adventure!  (Add some marketing copy explaining that to be a part of ISLA you HAVE TO sign up for the newsletter)', 'isla' ); ?></h5>
                            <button type="button" class="btn btn-primary col-margin-up">
                                <?php _e( 'CTA TEXT HERE', 'isla' ); ?>
                            </button>
                        </div>
                    </div>                     
                </div>                 
                <!-- Conteiner 7 - Background Image : End -->                                 

<?php get_footer( 'smallheader' ); ?>