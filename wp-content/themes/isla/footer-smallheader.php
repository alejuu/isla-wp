
</main>             
            <footer class="site-footer" id="footer"> 
                <!-- Conteiner 8 - Sponsors : Start -->                 
                <div class="container-fluid container-home-8 white-bg"> 
                    <div class="row"> 
                        <div class="col-md-12 text-center"> 
                            <img class="sponsor" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-sponsors-1.png" /> 
                            <img class="sponsor" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-sponsors-2.png" /> 
                            <img class="sponsor" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-sponsors-3.png" /> 
                            <img class="sponsor" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-sponsors-4.png" /> 
                            <img class="sponsor" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-sponsors-5.png" /> 
                            <img class="sponsor" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-sponsors-6.png" /> 
                            <img class="sponsor" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-sponsors-7.png" /> 
                            <img class="sponsor" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-sponsors-8.png" /> 
                            <img class="sponsor" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-sponsors-9.png" /> 
                            <img class="sponsor" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-sponsors-10.png" /> 
                        </div>                         
                    </div>                     
                </div>                 
                <!-- Conteiner 8 - Sponsors : End -->                 
                <nav class="navbar navbar-inverse navbar-footer" role="navigation"> 
                    <div class="container-fluid"> 
                        <div class="navbar-header"> 
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> 
                                <span class="sr-only"><?php _e( 'Toggle navigation', 'isla' ); ?></span> 
                                <span class="icon-bar"></span> 
                                <span class="icon-bar"></span> 
                                <span class="icon-bar"></span> 
                            </button>                             
                            <a class="navbar-brand" href="<?php echo esc_url( get_home_url() ); ?>"><strong class="white"><?php _e( 'ISLA', 'isla' ); ?></strong></a> 
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"> 
                            <?php wp_nav_menu( array(
                                  'menu' => 'menu-footer',
                                  'menu_class' => 'nav navbar-nav',
                                  'container' => '',
                                  'depth' => '2',
                                  'theme_location' => 'menu-footer',
                                  'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                                  'walker' => new wp_bootstrap_navwalker()
                            ) ); ?> 
                            <?php wp_nav_menu( array(
                                  'menu' => 'social-footer',
                                  'menu_class' => 'nav navbar-nav navbar-right',
                                  'container' => '',
                                  'depth' => '2',
                                  'theme_location' => 'social-footer',
                                  'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                                  'walker' => new wp_bootstrap_navwalker()
                            ) ); ?> 
                        </div>                         
                    </div>                     
                </nav>                 
                <h6><?php _e( '©2016 International Surf Lifesaving Association  ·  Terms of Use  ·  Privacy Policy', 'isla' ); ?></h6> 
            </footer>             
        </div>         
        <!-- Bootstrap core JavaScript
    ================================================== -->         
        <!-- Placed at the end of the document so the pages load faster -->                           
        <?php wp_footer(); ?>
    </body>     
</html>
