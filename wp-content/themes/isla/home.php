<?php
get_header(); ?>

        <!-- Conteiner 2 - White - Learn More About Us : Start -->                 
        <?php
  $image_attributes = (is_singular() || in_the_loop()) ? wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' ) : null;
  if( !$image_attributes  && ( $header_image = get_header_image() ) ) $image_attributes = array( $header_image );
?>
        <div class="hidden-xs" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>;color:<?php echo '#'.get_header_textcolor() ?>;"> 
            <div class="container-fluid container-home-1 dimmer jumbotron-inner"> 
                <div class="row">
                    <div class="text-headings"> 
                        <h3><?php _e( 'We are WATER PEOPLE', 'isla' ); ?></h3> 
                        <h3><?php _e( 'We are LIFEGUARDS', 'isla' ); ?></h3> 
                        <h3><?php _e( 'We are GLOBETROTTERS', 'isla' ); ?></h3> 
                        <h3><?php _e( 'We are ACTIVISTS', 'isla' ); ?></h3> 
                        <h2><?php _e( 'We are', 'isla' ); ?></h2> 
                        <h1><b><?php _e( 'ISLA', 'isla' ); ?></b></h1> 
                    </div>
                </div>                         
            </div>                     
        </div>
        <div class="container container-home-2">
            <div class="row ch2-bounceInUp invisible">
                <div class="col-md-4 ch2-col col-sm-4">
                    <p class="large"><?php _e( 'ISLA exists to advance professional lifesaving development to areas in need around the globe.', 'isla' ); ?></p>
                    <p class="h3 blue-darker"><?php _e( 'LEARN MORE ABOUT US', 'isla' ); ?></p>
                </div>
                <div class="col-md-8 ch2-col col-sm-8">
                    <p><?php _e( 'Out of a deep passion for open water and a strong desire to prevent drownings, ISLA aids people in championing the aquatic safety situation in their own coastal communities. We do this through lifeguard training programs, lifeguard exchanges, equipment donations, purchasing connections, and utilizing the latest technology to sustain a global network of lifeguards that share information, techniques, stories, and culture. Our wish is to see friends and partners around the globe have the necessary tools to keep their water safe.', 'isla' ); ?></p>
                </div>
            </div>
        </div>                 
        <!-- Conteiner 2 - White - Learn More About Us : End -->                 
        <!-- Conteiner 3 - Blue - Members : Start -->                 
        <section class="container-home-3 blue-bg"> 
            <div class="container"> 
                <div class="row ch3-bounceInUp invisible">
                    <div class="col-md-12 col-margin-down ch3-col text-center">
                        <h2 class="white"><?php _e( 'Be the change and join in three simple steps', 'isla' ); ?></h2>
                    </div>
                    <div class="col-md-4 col-members text-center col-sm-4">
                        <a href="#" class="thumbnail circle img-members">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-members-1.png" alt="" width="100%">
                        </a>
                        <h5 class="white"><?php _e( 'STEP 1: BECOME A MEMBER', 'isla' ); ?></h5>
                        <p class="white hidden-sm"><?php _e( 'YOU can join the ISLA comunity and support our mission to advance professional lifesaving in areas with alarming drowning rates; as well as help grow a culture of ocean safety across the globe.', 'isla' ); ?><br></p>
                    </div>
                    <div class="col-md-4 col-members text-center col-sm-4">
                        <a href="#" class="thumbnail circle img-members">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-members-2.png" alt="" width="100%">
                        </a>
                        <h5 class="white"><?php _e( 'STEP 2: SHOP', 'isla' ); ?></h5>
                        <p class="white hidden-sm"><?php _e( 'Once you have your sicq ISLA membership kit,  you\'ll be eligible to recieve prodeals (30-60% off) on the brands you want, and discounts on sweet swag at our ISLA Surf Surf Shop.', 'isla' ); ?><br></p>
                    </div>
                    <div class="col-md-4 col-members text-center col-sm-4">
                        <a href="#" class="thumbnail circle img-members">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-members-3.png" alt="" width="100%">
                        </a>
                        <h5 class="white"><?php _e( 'STEP 3: PARTICIPATE', 'isla' ); ?></h5>
                        <p class="white hidden-sm"><?php _e( 'We\'re looking for lifeguards, firefighters, police officers, nurses, doctors, paramedics, ski patrollers, EMT’s, translators, photographers, &amp; travel writters to join us on our humanitarian projects.', 'isla' ); ?><br></p>
                    </div>
                    <div class="col-md-12 col-margin-up text-center">
                        <button type="button" class="btn btn-primary">
                            <?php _e( 'Get Involved', 'isla' ); ?>
                        </button>
                    </div>
                </div>                         
            </div>                     
        </section>                 
        <!-- Conteiner 3 - Blue - Members : End -->                 
        <!-- Conteiner 4 - Grey - Projects : Start -->                 
        <section class="container-home-4 gray-darker-bg"> 
            <div class="container">
                <div class="row ch4-bounceInUp invisible">
                    <div class="col-md-12 col-margin-down text-center">
                        <h2 class="gray-lighter"><?php _e( 'Projects', 'isla' ); ?></h2>
                    </div>
                    <?php
                        $projects_args = array(
                          'cat' => 'projects',
                          'category_name' => 'projects',
                          'posts_per_page' => '3'
                        )
                    ?>
                    <?php $projects = new WP_Query( $projects_args ); ?>
                    <?php if ( $projects->have_posts() ) : ?>
                        <div class="col-md-12 col-sm-12">
                            <div class="row">
                                <?php while ( $projects->have_posts() ) : $projects->the_post(); ?>
                                    <article> 
                                        <div class="col-md-4 col-sm-4 text-center"> 
                                            <a href="<?php echo esc_url( wp_get_shortlink()); ?>"> 
                                                <?php $image_attributes = (is_singular() || in_the_loop()) ? wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' ) : null; ?>
                                                <div class="img-projects" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>"></div>                                                         
                                            </a>                                                     
                                            <a href="<?php echo esc_url( wp_get_shortlink()); ?>"><h3 class="gray-lighter text-uppercase"><?php the_title(); ?></h3></a> 
                                            <div class="center-divider"></div>                                                     
                                            <div class="gray-light"> 
                                                <?php the_excerpt( ); ?> 
                                            </div>                                                     
                                        </div>                                                 
                                    </article>
                                <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            </div>                                     
                        </div>
                    <?php endif; ?>
                    <!-- <div class="col-md-4 text-center col-sm-4">
        <a href="#">
          <div class="img-projects"></div>
        </a>
        <h3 class="gray-lighter">Column title</h3>
        <p class="gray-light">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
      </div>
      <div class="col-md-4 text-center col-sm-4">
        <a href="#">
          <div class="img-projects"></div>
        </a>
        <h3 class="gray-lighter">Column title</h3>
        <p class="gray-light">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
      </div>
      <div class="col-md-4 text-center col-sm-4">
        <a href="#">
          <div class="img-projects"></div>
        </a>
        <h3 class="gray-lighter">Column title</h3>
        <p class="gray-light">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
      </div>                -->
                </div>
            </div>
        </section>                 
        <!-- Conteiner 4 - Grey - Projects : End -->                 
        <!-- Conteiner 5 - White - News : Start -->                 
        <div class="container container-home-5"> 
            <div class="row ch5-bounceInUp invisible"> 
                <div class="col-md-12 col-margin-down text-center">
                    <h2><?php _e( 'ISLA NEWS', 'isla' ); ?></h2>
                </div>                         
                <?php
                    $news_args = array(
                      'cat' => 'news',
                      'category_name' => 'news',
                      'posts_per_page' => '4'
                    )
                ?>
                <?php $news = new WP_Query( $news_args ); ?>
                <?php if ( $news->have_posts() ) : ?>
                    <div class="col-md-12 col-sm-12">
                        <div class="row">
                            <?php while ( $news->have_posts() ) : $news->the_post(); ?>
                                <article>
                                    <div class="col-md-6 col-projects padding-twenty col-sm-6 col-xs-12">
                                        <div class="row">
                                            <div class="col-md-5 col-sm-12"> 
                                                <a href="<?php echo esc_url( wp_get_shortlink()); ?>"> 
                                                    <?php $image_attributes = (is_singular() || in_the_loop()) ? wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' ) : null; ?>
                                                    <div class="img-news" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>"></div>                                                             
                                                </a>                                                         
                                            </div>
                                            <div class="col-md-7 col-sm-12"> 
                                                <h6><?php echo get_the_time( get_option( 'date_format' ) ) ?></h6> 
                                                <a href="<?php echo esc_url( wp_get_shortlink()); ?>"><h4><?php the_title(); ?></h4></a> 
                                                <div class="divider"></div>                                                         
                                                <?php the_excerpt( ); ?> 
                                            </div>
                                        </div>                                                 
                                    </div>                                             
                                </article>
                            <?php endwhile; ?>
                            <?php wp_reset_postdata(); ?>
                        </div>                                 
                    </div>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'isla' ); ?></p>
                <?php endif; ?> 
                <div class="col-md-12 col-margin-up text-center">
                    <button type="button" class="btn btn-primary">
                        <?php _e( 'Read More', 'isla' ); ?>
                    </button>
                </div>                         
            </div>                     
        </div>                 
        <!-- Conteiner 5 - White - News : End -->                 
        <!-- Conteiner 6 - Gray Lighter - Store : Start -->                 
        <section class="container-home-6 gray-lighter-bg"> 
            <div class="container"> 
                <div class="row ch6-bounceInUp invisible"> 
                    <div class="col-md-4 col-sm-12">
                        <h5 class="text-uppercase"><?php _e( 'Featured Products', 'isla' ); ?></h5>
                        <h2><?php _e( 'ISLA Surf Shop', 'isla' ); ?></h2>
                        <p><?php _e( 'We provide the lowest price on products that are tested in every terrain and condition. Proceeds fund ISLA projects around the globe.', 'isla' ); ?></p>
                        <p><a href=""><?php _e( 'Become an ISLA member', 'isla' ); ?></a> <?php _e( 'to immediately access special pricing on premium apparel, tech gear, swag and more!', 'isla' ); ?></p>
                    </div>                             
                    <?php
                        $store_args = array(
                          'post_type' => 'product',
                          'posts_per_page' => '3'
                        )
                    ?>
                    <?php $store = new WP_Query( $store_args ); ?>
                    <?php if ( $store->have_posts() ) : ?>
                        <div class="col-md-8 col-sm-12">
                            <div class="row">
                                <?php while ( $store->have_posts() ) : $store->the_post(); ?>
                                    <article>
                                        <div class="col-md-4 text-center col-sm-12">
                                            <?php the_post_thumbnail( null, array(
                                                  'class' => 'img-responsive center-img'
                                            ) ); ?>
                                            <h3><?php the_title(); ?></h3>
                                            <h5><?php echo $product->get_price_html(); ?></h5>
                                        </div>
                                    </article>
                                <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            </div>
                        </div>
                    <?php else : ?>
                        <p><?php _e( 'Sorry, no posts matched your criteria.', 'isla' ); ?></p>
                    <?php endif; ?> 
                </div>                         
            </div>
        </section>                 
        <!-- Conteiner 6 - Gray Lighter - Store : End -->                 
        <!-- Conteiner 7 - Background Image : Start -->                 
        <div class="container-fluid container-home-7"> 
            <div class="row ch7-bounceInUp invisible">
                <div class="col-md-12 text-center">
                    <h2 class="white"><?php _e( 'Interested in traveling the globe as a humanitarian lifeguard?', 'isla' ); ?></h2>
                    <h5 class="white"><?php _e( 'Congratulations, you’re at the first step to your ISLA adventure!  (Add some marketing copy explaining that to be a part of ISLA you HAVE TO sign up for the newsletter)', 'isla' ); ?></h5>
                    <button type="button" class="btn btn-primary col-margin-up">
                        <?php _e( 'CTA TEXT HERE', 'isla' ); ?>
                    </button>
                </div>
            </div>
        </div>                 
        <!-- Conteiner 7 - Background Image : End -->                                 

<?php get_footer(); ?>