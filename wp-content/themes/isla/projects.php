<?php
/*
 Template Name: projects
 */
?>
<?php
get_header( 'smallheader' ); ?>

<!-- Conteiner 1 - Image : Start -->
<div class="container-fluid container-category-1">
    <div class="container text-center">
        <h1 class="text-uppercase white"><?php _e( 'ISLA International Upcoming Development Projects', 'isla' ); ?><br></h1>
    </div>
</div>
<?php
    $Featured_args = array(
      'cat' => 'featured',
      'category_name' => 'featured',
      'nopaging' => true
    )
?>
<?php $Featured = new WP_Query( $Featured_args ); ?>
<?php if ( $Featured->have_posts() ) : ?>
    <div>
        <?php while ( $Featured->have_posts() ) : $Featured->the_post(); ?>
            <div class="container-fluid container-category-2 featured-category">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <article>
                                <div class="col-md-6 col-sm-6 col-featured-left">
                                    <div class="circle-badge" style="visibility:<?php echo get_post_meta( get_the_ID(), 'badge', true ); ?>;">
                                        <p><?php echo get_post_meta( get_the_ID(), 'project_status', true ); ?></p>
                                    </div>
                                    <a href="<?php echo esc_url( wp_get_shortlink()); ?>">
                                        <?php $image_attributes = (is_singular() || in_the_loop()) ? wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' ) : null; ?>
                                        <div class="img-category-page" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>"></div>
                                    </a>
                                </div>
                                <div class="col-md-6 col-sm-6 col-featured-right">
                                    <h5 class="text-uppercase gray-light"><?php echo get_post_meta( get_the_ID(), 'project_date', true ); ?></h5>
                                    <a href="<?php echo esc_url( wp_get_shortlink()); ?>"><h2><?php the_title(); ?></h2></a>
                                    <p><?php the_excerpt( ); ?></p>
                                    <h6 class="category-status-title text-uppercase"><?php _e( 'Status', 'isla' ); ?></h6>
                                    <p class="category-status-desc no-margin"><?php echo get_post_meta( get_the_ID(), 'project_status_desc', true ); ?></p>
                                    <p class="no-margin"><?php echo get_post_meta( get_the_ID(), 'project_app_due', true ); ?></p>
                                    <p><?php echo get_post_meta( get_the_ID(), 'project_volunteers', true ); ?></p>
                                    <div class="follow-along-category" style="visibility:<?php echo get_post_meta( get_the_ID(), 'follow_along', true ); ?>;">
                                        <p><?php _e( 'Follow along:', 'isla' ); ?></p>
                                        <a href="<?php echo esc_url( get_post_meta( get_the_ID(), 'follow_fb', true ) ); ?>" target="_blank"><i class="icon ion-social-facebook blue"></i></a>
                                        <a href="<?php echo esc_url( get_post_meta( get_the_ID(), 'follow_twitter', true ) ); ?>" target="_blank"><i class="icon ion-social-twitter blue"></i></a>
                                        <a href="<?php echo esc_url( get_post_meta( get_the_ID(), 'follow_instagram', true ) ); ?>" target="_blank"><i class="icon ion-social-instagram-outline blue"></i></a>
                                    </div>
                                    <a href="<?php echo esc_url( get_post_meta( get_the_ID(), 'project_details_link', true ) ); ?>"><h4 class="blue"><?php _e( 'Project Details >', 'isla' ); ?></h4></a>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
    </div>
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.', 'isla' ); ?></p>
<?php endif; ?>
<!-- Conteiner 1 - Image : End -->
<!-- Conteiner 2 - Grey Lighter - Chile : Start -->
<!-- Conteiner 2 - Grey Lighter - Chile : End -->
<!-- Conteiner 3 - White - Nicaragua : Start -->
<!-- Conteiner 3 - White - Nicaragua : End -->
<!-- Conteiner 4 - Grey Lighter - Europe : Start -->
<!-- Conteiner 4 - Grey Lighter - Europe : End -->
<!-- Conteiner 5 - White - Mexico : Start -->
<!-- Conteiner 5 - White - Mexico : End -->
<!-- Conteiner 6 - Blue - Apply : Start -->
<div class="container-fluid container-category-3 blue-bg">
    <div class="container">
        <div class="row ch5-bounceInUp invisible">
            <div class="col-md-12">
                <h2 class="white"><?php _e( 'Want to apply for a project?', 'isla' ); ?></h2>
                <p class="white"><?php _e( 'Our open project application periods are announced individually throughout the year via our email newsletter. Volunteer selection is HIGHLY competitive, so it is best to appliy as soon as the project application opens. Sign up for the ISLA newsletter to ensure you don’t get left behind!', 'isla' ); ?></p>
                <button type="button" class="btn btn-transparent text-uppercase">
                    <?php _e( 'Sign Up Now', 'isla' ); ?>
                </button>
                <h2 class="white"><?php _e( 'Already subscribed to our newsletter?', 'isla' ); ?></h2>
                <p class="white"><?php _e( 'While you’re waiting for additional inforamtion on a project, follow and engage ISLA on its social media outlets.', 'isla' ); ?></p>
                <p class="white"><b><?php _e( 'Share. Comment. Tweet. Like. Repeat.', 'isla' ); ?></b></p>
                <p class="white"><?php _e( 'Not only will you get any answers you’re looking for, but you’ll create awareness for the global drowning epidemic, and help us sustain and acquire corporate sponsors... which in turn means lower trip costs, more certification courses, bigger discounts on gear, and free swag for you!', 'isla' ); ?></p>
            </div>
        </div>
    </div>
</div>
<!-- Conteiner 6 - Blue - Apply : End -->                

<?php get_footer( 'smallheader' ); ?>