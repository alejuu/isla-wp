<!doctype html> 
<html <?php language_attributes(); ?>> 
    <head> 
        <meta charset="<?php bloginfo( 'charset' ); ?>"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <meta name="author" content="Pinegrow Web Editor">          
        <!-- Bootstrap core CSS -->         
        <!-- Custom styles for this template -->         
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->         
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->                  
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
    </head>     
    <body class="<?php echo implode(' ', get_body_class()); ?>"> 
        <div class="site-container"> 
            <header class="site-header" id="masthead"> 
                <nav> 
                    <!-- Main Navbar : Start -->                     
                    <nav class="navbar-inverse navbar-main navbar-fixed-top hidden-xs" role="navigation"> 
                        <div class="container-fluid"> 
                            <div class="navbar-header"> 
                                <a class="navbar-brand site-title visible-sm" href="<?php echo esc_url( get_home_url() ); ?>"><?php bloginfo( 'name' ); ?></a> 
                                <?php pg_starter_the_custom_logo() ?> 
                            </div>
                            <div class="navbar-main-menu"> 
                                <?php wp_nav_menu( array(
                                      'menu' => 'primary',
                                      'menu_class' => 'nav navbar-nav',
                                      'container' => '',
                                      'depth' => '2',
                                      'theme_location' => 'primary',
                                      'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                                      'walker' => new wp_bootstrap_navwalker()
                                ) ); ?> 
                                <ul class="nav navbar-nav navbar-right"> 
                                    <li> 
                                        <a href="#"> 
                                            <button type="button" class="btn btn-default">
                                                <?php _e( 'Donate Now', 'isla' ); ?>
                                            </button>                                             
                                        </a>                                         
                                    </li>                                     
                                </ul>                                 
                                <?php wp_nav_menu( array(
                                      'menu' => 'social',
                                      'menu_class' => 'nav navbar-nav navbar-right',
                                      'container' => '',
                                      'depth' => '2',
                                      'theme_location' => 'social',
                                      'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                                      'walker' => new wp_bootstrap_navwalker()
                                ) ); ?> 
                            </div>                             
                        </div>                         
                    </nav>                     
                    <!-- Main Navbar : End -->                     
                    <!-- Secondary Navbar: Start -->                     
                    <nav class="navbar-secondary navbar-fixed-top hidden-xs" role="navigation"> 
                        <div class="container-fluid"> 
                            <?php wp_nav_menu( array(
                                  'menu' => 'secondary',
                                  'menu_class' => 'nav navbar-nav navbar-right',
                                  'container' => '',
                                  'depth' => '2',
                                  'theme_location' => 'secondary',
                                  'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                                  'walker' => new wp_bootstrap_navwalker()
                            ) ); ?> 
                        </div>                         
                    </nav>                     
                    <!-- Secondary Navbar : End -->                     
                    <!-- Mobile Navbar : Start -->                     
                    <div class="visible-xs"> 
                        <button type="button" class="navbar-toggle navbar-mob-toggle" data-toggle="offcanvas" data-target="sidebar"> 
                            <span class="icon-bar white-bg"></span> 
                            <span class="icon-bar white-bg"></span> 
                            <span class="icon-bar white-bg"></span> 
                        </button>                         
                        <nav class="navbar-mob col-xs-6 col-sm-3" id="sidebar" role="navigation"> 
                            <div class="navbar-header"> 
                                <a class="navbar-brand text-center" href="#"> <h6><?php _e( 'International Surf Lifesaving Association', 'isla' ); ?></h6> </a> 
                            </div>                             
                            <div class="sidebar-nav"> 
                                <?php wp_nav_menu( array(
                                      'menu' => 'mobile',
                                      'menu_class' => 'nav navbar-nav',
                                      'container' => '',
                                      'depth' => '2',
                                      'theme_location' => 'mobile',
                                      'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                                      'walker' => new wp_bootstrap_navwalker()
                                ) ); ?> 
                            </div>                             
                        </nav>                         
                    </div>                     
                    <!-- Mobile Navbar : End -->                     
                </nav>                 
                <div class="container-fluid breadcrumbs-section" id="more"> 
                    <div class="row"> 
                        <div class="col-md-12"> 
                            <div class="breadcrumbs-cnt"> 
                                <?php get_template_part( 'assets/breadcrumbs/breadcrumb' ); ?> 
                            </div>                             
                        </div>                         
                    </div>                     
                </div>                 
                <?php if ( is_singular() ) : ?>
                    <?php wp_enqueue_script( 'comment-reply' ); ?>
                <?php endif; ?> 
            </header>             
            <main class="site-inner site-content">