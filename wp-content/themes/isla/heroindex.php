<?php
/*
 Template Name: hero post
 */
?>
<?php
get_header( 'smallheader' ); ?>

<?php $image_attributes = (is_singular() || in_the_loop()) ? wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ) ) : null; ?>
<div class="container-fluid text-center container-center-full full-hero" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>">
    <h1 class="white"><?php the_title(); ?></h1>
</div>                 
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-9 col-xs-12">
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php get_template_part( 'template-parts/content-single' ); ?>
                <?php endwhile; ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.', 'isla' ); ?></p>
            <?php endif; ?> 
            <ul class="pager posts-navigation text-uppercase"> 
                <?php if ( get_next_posts_link() ) : ?>
                    <li class="previous"> 
                        <?php next_posts_link( 'Older Posts' ); ?> 
                    </li>
                <?php endif; ?> 
                <?php if ( get_previous_posts_link() ) : ?>
                    <li class="next"> 
                        <?php previous_posts_link( __( 'Newer Posts', 'isla' ) ); ?> 
                    </li>
                <?php endif; ?> 
            </ul>                             
        </div>                         
        <div class="col-sm-3 col-xs-12"> 
            <?php if ( is_active_sidebar( 'right_sidebar' ) ) : ?>
                <div id="main_sidebar">
                    <?php dynamic_sidebar( 'right_sidebar' ); ?>
                </div>
            <?php endif; ?> 
        </div>                         
    </div>                     
</div>                                 

<?php get_footer( 'smallheader' ); ?>