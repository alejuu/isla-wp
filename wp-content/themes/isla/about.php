<?php
/*
 Template Name: about
 */
?>
<?php
get_header( 'smallheader' ); ?>

<!-- Conteiner 1 - Full Bg Image : Start -->                 
<section class="container-fluid container-about-1 container-center-full"> 
    <div class="container"> 
        <div class="row"> 
            <div class="col-md-12 text-center">
                <h1 class="text-uppercase white"><?php _e( 'About ISLA', 'isla' ); ?></h1>
                <h3 class="white"><?php _e( 'Que inem morum ussulti lientra nocuperio uterum or audaciae, que firium ius, nos more, quius et fero caveri spimo ia novivid Cuperib untimis restractum publiusquid.', 'isla' ); ?></h3>
            </div>
            <div class="col-md-3 col-sm-6 text-center">
                <a href="#volunteers">
                    <button type="button" class="btn btn-transparent text-uppercase responsive-btn">
                        <?php _e( 'Volunteers', 'isla' ); ?>
                    </button>
                </a>
            </div>                             
            <div class="col-md-3 col-sm-6 text-center">
                <a href="#beginnings">
                    <button type="button" class="btn btn-transparent text-uppercase responsive-btn">
                        <?php _e( 'ISLA’s Beginnings', 'isla' ); ?>
                    </button>
                </a>
            </div>                             
            <div class="col-md-3 col-sm-6 text-center">
                <a href="#stats">
                    <button type="button" class="btn btn-transparent text-uppercase responsive-btn">
                        <?php _e( 'Our Stats', 'isla' ); ?>
                    </button>
                </a>
            </div>                             
            <div class="col-md-3 col-sm-6 text-center">
                <a href="#directors">
                    <button type="button" class="btn btn-transparent text-uppercase responsive-btn">
                        <?php _e( 'Board of Directors', 'isla' ); ?>
                    </button>
                </a>
            </div>                             
        </div>                         
    </div>                     
</section>                 
<!-- Conteiner 1 - Full Bg Image : End -->                 
<!-- Conteiner 2 - White - Learn More About Us : Start -->                 
<div class="container container-about-2"> 
    <div class="row ch2-bounceInUp invisible"> 
        <div class="col-md-12 col-margin-down text-center">
            <h2 class="blue"><?php _e( 'ISLA exists to advance professional lifesaving development to areas in need around the globe.', 'isla' ); ?></h2>
        </div>                         
        <div class="col-md-12 col-margin-down">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 no-padding text-center col-xs-6">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-about-img-1.jpg" class="img-responsive center-img" />
                        </div>
                        <div class="col-md-6 col-sm-6 no-padding text-center col-xs-6">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-about-img-2.jpg" class="img-responsive center-img" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <h3><?php _e( 'Out of a deep passion for open water and a strong desire to prevent drownings, ISLA aids people in championing the aquatic safety situation in their own coastal communities.', 'isla' ); ?></h3>
                    <p><?php _e( 'We do this through lifeguard training programs, lifeguard exchanges, equipment donations, purchasing connections, and utilizing the latest technology to sustain a global network of lifeguards that share information, techniques, stories, and culture. Our wish is to see friends and partners around the globe have the necessary tools to keep their water safe.', 'isla' ); ?></p>
                </div>
            </div>                             
        </div>                         
        <div class="col-md-12 col-margin-down">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-md-push-6">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 no-padding text-center col-xs-6">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-about-img-3.jpg" class="img-responsive center-img" />
                        </div>
                        <div class="col-md-6 col-sm-6 no-padding text-center col-xs-6">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-about-img-4.jpg" class="img-responsive center-img" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-md-pull-6">
                    <h3><?php _e( 'ISLA is a social justice-driven organization that is young, innovative, and not afraid to do things for the first time.', 'isla' ); ?></h3>
                    <p><?php _e( 'We are ultimately guided by the infamous mantra: “Safety third, winning and looking good are number one and two!” We adventure the globe with purpose, seeking to share life and help protect it.', 'isla' ); ?></p>
                </div>
            </div>                             
        </div>
        <div class="col-md-12 col-margin-down">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 no-padding text-center col-xs-6">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-about-img-5.jpg" class="img-responsive center-img" />
                        </div>
                        <div class="col-md-6 col-sm-6 no-padding text-center col-xs-6">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-about-img-6.jpg" class="img-responsive center-img" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <h3><?php _e( 'The global drowning epidemic compels us to action as we pioneer new ways to think about drowning in the developing world and effective solutions that will save thousands of lives.', 'isla' ); ?></h3>
                    <p><?php _e( 'ISLA uses both ground-breaking technology and tried and true techniques; we follow in the tradition of those before us without fear to push it to a whole new level, constantly seeking to improve ourselves and the work we do. We welcome challenges and strive to leave a positive impact wherever we go.', 'isla' ); ?></p>
                </div>
            </div>                             
        </div>                         
        <div class="col-md-12 col-margin-down">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-md-push-6">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 no-padding text-center col-xs-6">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-about-img-7.jpg" class="img-responsive center-img" />
                        </div>
                        <div class="col-md-6 col-sm-6 text-center no-padding col-xs-6">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-about-img-8.jpg" class="img-responsive center-img" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-md-pull-6">
                    <h3><?php _e( 'Our team lives on the edge, is ready for any adventure, and is prepared for any situation.', 'isla' ); ?></h3>
                    <p><?php _e( 'We live simply and we work hard, but that doesn’t stop us from watching the sun rise twice without sleeping, exploring new lands, surfing down volcanos, zip lining across the jungle, climbing into ancient caves, and trying the local brews with our new friends, all the while contributing to drowning prevention.', 'isla' ); ?></p>
                </div>
            </div>                             
        </div>                         
        <div class="col-md-12 col-margin-down text-center blue">
            <h2 class="blue"><?php _e( 'We are water people. We are lifeguards. We are globetrotters. We are activists.', 'isla' ); ?></h2>
            <h2 class="blue text-uppercase"><?php _e( 'We are ISLA', 'isla' ); ?></h2>
            <button type="button" class="btn btn-primary hidden-sm hidden-xs">
                <?php _e( 'Learn more about the global drowning epidemic', 'isla' ); ?>
            </button>
        </div>                         
    </div>                     
</div>                 
<!-- Conteiner 2 - White - Learn More About Us : End -->                 
<!-- Conteiner 3 - Gray Dark - Volunteers : Start -->                 
<section class="container-about-3 gray-dark-bg" id="volunteers"> 
    <div class="row">
        <div class="container"> 
            <div class="row ch3-bounceInUp invisible">
                <div class="col-md-12 col-margin-down text-center">
                    <h2 class="white"><?php _e( 'Volunteers', 'isla' ); ?></h2>
                </div>
                <div class="col-md-6 col-sm-12">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-about-volunteers.jpg" alt="" width="100%">
                    <caption class="white">
                        <?php _e( 'Image caption with names for 7 OG volunteers', 'isla' ); ?>
                    </caption>
                </div>
                <div class="col-md-6 col-sm-12">
                    <p class="white"><?php _e( 'Our volunteer base started with seven Junior Lifeguard Instructors from Huntington Beach, CA. Little did we know the vast amount of people from other lifeguarding agencies had the same idea — so they joined us!', 'isla' ); ?></p>
                    <p class="white"><?php _e( 'ISLA has since grown into a dynamic team of lifeguards, doctors, nurses, firefighters, paramedics, and emergency & military personnel from various agencies throughout the globe!', 'isla' ); ?></p>
                    <p class="white"><?php _e( 'Our operations started in Nicaragua, and have expanded into many countries in providing aid, support, and exchange programs in an effort to raise awareness of the drowning epidemic and save lives.', 'isla' ); ?></p>
                </div>
                <div class="col-md-12 col-margin-up text-center">
                    <button type="button" class="btn btn-primary no-border hidden-sm hidden-xs">
                        <?php _e( 'The ISLA team and how you can get involved', 'isla' ); ?>
                    </button>
                </div>
            </div>                             
        </div>
    </div>                     
</section>                 
<!-- Conteiner 3 - Gray Dark - Volunteers : End -->                 
<!-- Conteiner 4 - Gray Lighter - Beginnings : Start -->                 
<div class="container-fluid container-about-4 gray-lighter-bg" id="beginnings"> 
    <div class="row">
        <div class="container"> 
            <div class="row ch3-bounceInUp invisible"> 
                <div class="col-md-12 col-margin-down text-center">
                    <h2><?php _e( 'ISLA’s Beginnings', 'isla' ); ?></h2>
                </div>                                 
                <div class="col-md-6 col-sm-12">
                    <h3><?php _e( 'In 2008, four Huntington Beach Junior Lifeguard Instructors — Peter Eich, Scott Hunthausen, Olin Patterson, and Henry Reyes — wanted something to bond their friendships for life and to continue their for passion lifeguarding.', 'isla' ); ?></h3>
                    <p><?php _e( 'The idea started as a realization when Scott returned home from a study abroad semester in Nicaragua where he experienced the drowning of his host family’s son, and witnessed the alarming drowning rates in the country during the 4-day Semana Santa holiday (Easter).', 'isla' ); ?></p>
                    <caption>
                        <?php _e( 'We need some wrap-up text here.', 'isla' ); ?> 
                        <b><?php _e( 'Thus began ISLA', 'isla' ); ?></b>
                        <?php _e( '.', 'isla' ); ?> 
                    </caption>
                </div>                                 
                <div class="col-md-6 col-sm-12">
                    <img src="" alt="" width="100%">
                    <caption>
                        <?php _e( 'Image caption with names for 7 OG volunteers', 'isla' ); ?>
                    </caption>
                </div>                                 
                <div class="col-md-12 col-sm-12" id="stats"> 
                    <div class="col-md-12 col-margin text-center">
                        <h3><?php _e( 'Since its beginnings in 2008, ISLA has:', 'isla' ); ?></h3>
                    </div>                                     
                    <div class="col-md-3 text-center col-sm-6 col-xs-12 padding-5"> 
                        <div class="circle beginnings-circle blue-darker-bg center-div">
                            <div class="container-center">
                                <h4 class="white"><?php _e( 'Conducted', 'isla' ); ?></h4>
                                <h1 class="white"><?php _e( '10', 'isla' ); ?></h1>
                                <h4 class="white"><?php _e( 'lifeguard training courses', 'isla' ); ?></h4>
                            </div>
                        </div>                                         
                    </div>                                     
                    <div class="col-md-3 text-center col-sm-6 col-xs-12 padding-5"> 
                        <div class="circle beginnings-circle blue-darker-bg center-div">
                            <div class="container-center">
                                <h4 class="white"><?php _e( 'Performed', 'isla' ); ?></h4>
                                <h1 class="white"><?php _e( '207', 'isla' ); ?></h1>
                                <h4 class="white"><?php _e( 'rescues lorem ipsum dolor', 'isla' ); ?></h4>
                            </div>
                        </div>                                         
                    </div>                                     
                    <div class="col-md-3 text-center col-sm-6 col-xs-12 padding-5"> 
                        <div class="circle beginnings-circle blue-darker-bg center-div">
                            <div class="container-center">
                                <h4 class="white"><?php _e( '(verb)', 'isla' ); ?></h4>
                                <h1 class="white"><?php _e( '1,422', 'isla' ); ?></h1>
                                <h4 class="white"><?php _e( 'preventative actions', 'isla' ); ?></h4>
                            </div>
                        </div>                                         
                    </div>                                     
                    <div class="col-md-3 text-center col-sm-6 col-xs-12 padding-5"> 
                        <div class="circle beginnings-circle blue-darker-bg center-div">
                            <div class="container-center">
                                <h4 class="white"><?php _e( '(verb)', 'isla' ); ?></h4>
                                <h1 class="white"><?php _e( '180', 'isla' ); ?></h1>
                                <h4 class="white"><?php _e( 'medical aids in 6 countries', 'isla' ); ?></h4>
                            </div>
                        </div>                                         
                    </div>                                     
                    <div class="col-md-12 col-margin-up text-center">
                        <button type="button" class="btn btn-primary no-border hidden-sm hidden-xs">
                            <?php _e( 'View ISLA’s Global Operations', 'isla' ); ?>
                        </button>
                    </div>                                     
                </div>                                 
            </div>                             
        </div>
    </div>                     
</div>                 
<!-- Conteiner 4 - Gray Lighter - Beginnings : End -->                 
<!-- Conteiner 5 - White - Directors : Start -->                 
<div class="container-fluid container-about-5" id="directors"> 
    <div class="row">
        <div class="container"> 
            <div class="row ch4-bounceInUp invisible"> 
                <div class="col-md-12 col-margin-down text-center">
                    <h2><?php _e( 'ISLA’s Board of Directors', 'isla' ); ?></h2>
                </div>                                 
                <?php if( have_rows('director') ): ?> 
                    <?php while( have_rows('director') ): the_row(); ?> 
                        <div class="col-md-12 col-margin-down"> 
                            <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                    <?php $image = get_sub_field('director_pic_one');  if( !empty($image) ): ?>
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="director-pic" />
                                <?php endif; ?>
                                </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                    <?php $image = get_sub_field('director_pic_two');  if( !empty($image) ): ?>
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="director-pic" />
                                <?php endif; ?>
                                </div>
                            </div>                                             
                            <div class="col-md-6 col-sm-12">
                                <h3><?php the_sub_field('director_name'); ?></h3>
                                <h6 class="text-uppercase"><?php the_sub_field('director_title'); ?></h6>
                                <p><?php the_sub_field('director_desc'); ?></p>
                            </div>                                             
                        </div>                                         
                    <?php endwhile; ?> 
                <?php endif; ?> 
            </div>                             
        </div>
    </div>                     
</div>
<div class="container-fluid container-about-5 gray-lighter-bg" id="directors"> 
    <div class="row">
        <div class="container"> 
            <div class="row ch4-bounceInUp invisible"> 
                <div class="col-md-12 col-margin-down text-center">
                    <h2><?php _e( 'Administrators', 'isla' ); ?></h2>
                </div>                                 
                <?php if( have_rows('admin') ): ?> 
                    <?php while( have_rows('admin') ): the_row(); ?> 
                        <div class="col-md-12 col-margin-down"> 
                            <div class="col-md-6 col-sm-12 col-xs-12 col-md-push-6">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                    <?php $image = get_sub_field('admin_pic_one');  if( !empty($image) ): ?>
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="director-pic" />
                                <?php endif; ?>
                                </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                    <?php $image = get_sub_field('admin_pic_two');  if( !empty($image) ): ?>
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="director-pic" />
                                <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-md-pull-6">
                                <h3><?php the_sub_field('admin_name'); ?></h3>
                                <h6 class="text-uppercase"><?php the_sub_field('admin_title'); ?></h6>
                                <p><?php the_sub_field('admin_desc'); ?></p>
                            </div>                                             
                        </div>                                         
                    <?php endwhile; ?> 
                <?php endif; ?> 
            </div>                             
        </div>
    </div>                     
</div>                 
<!-- Conteiner 5 - White - Directors : End -->                 
<!-- Conteiner 6 - Blue - Interested : Start -->                 
<div class="container-fluid container-about-6 blue-bg"> 
    <div class="row ch5-bounceInUp invisible">
        <div class="col-md-12 text-center">
            <h2 class="white"><?php _e( 'Interested in joining our Board of Directors?', 'isla' ); ?></h2>
            <h4 class="white"><?php _e( 'ISLA’s looking for skilled professionals that are passionate about our mission and willing to provide financial resources to operate and grow our organization. Inquire today about becoming an ISLA Director!', 'isla' ); ?></h4>
            <button type="button" class="btn btn-primary col-margin">
                <?php _e( 'CTA Text Here', 'isla' ); ?>
            </button>
        </div>
    </div>                     
</div>                 
<!-- Conteiner 6 - Blue - Interested : End -->                                 

<?php get_footer( 'smallheader' ); ?>