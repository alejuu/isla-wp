<?php
/**
 * 	Plugin Name: 	WordPress Functionality Plugin
 * 	Plugin URI: 	http://pinegrow.com
 * 	Description: 	Core WordPress customizations that are theme independent.
 * 	Author: 		Pinegrow Team
 * 	Author URI: 	http://pinegrow.com
 *
 *
 * 	Version: 		1.2
 * 	License: 		GPLv2
 *
 * Fork of Rick R. Duncan WordPress Functionality Plugin
 * https://gist.github.com/rickrduncan/2ffd0c25baefb7ac702b#file-wordpress-functionality-plugin-php
 *
 *  WordPress Functionality Plugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  any later version.
 *
 *  WordPress Functionality Plugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with WP Functional Plugin. If not, see <http://www.gnu.org/licenses/>.
 */


//* Remove 'Editor' from 'Appearance' Menu.
//* This stops users and hackers from being able to edit files from within WordPress.
define( 'DISALLOW_FILE_EDIT', true );


//* Add the ability to use shortcodes in widgets
add_filter( 'widget_text', 'do_shortcode' );


//* Prevent WordPress from compressing images
add_filter( 'jpeg_quality', create_function( '', 'return 100;' ) );


//* Disable any and all mention of emoji's
//* Source code credit: http://ottopress.com/
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );


//* Remove items from the <head> section
remove_action( 'wp_head', 'wp_generator' );							//* Remove WP Version number
remove_action( 'wp_head', 'wlwmanifest_link' );						//* Remove wlwmanifest_link
remove_action( 'wp_head', 'rsd_link' );								//* Remove rsd_link
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );			//* Remove shortlink
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );	//* Remove previous/next post links


//* Limit the number of post revisions to keep
add_filter( 'wp_revisions_to_keep', 'pg_set_revision_max', 10, 2 );
function pg_set_revision_max( $num, $post ) {

    $num = 5; //change 5 to match your preferred number of revisions to keep
    return $num;

}

/** ---:[ place your custom code below this line ]:--- */

//* Login Screen: Change login logo
add_action( 'login_head', 'pg_custom_login_logo' );
function pg_custom_login_logo() {
	echo '<style type="text/css">
    h1 a { background-image:url('.get_stylesheet_directory_uri().'/images/login-logo.png) !important; background-size: 285px 53px !important;height: 53px !important; width: 285px !important; margin-bottom: 0 !important; padding-bottom: 0 !important; }
    .login form { margin-top: 10px !important; }
    </style>';
}


//* Login Screen: Use your own URL for login logo link
add_filter( 'login_headerurl', 'pg_url_login' );
function pg_url_login(){

	return get_bloginfo( 'wpurl' ); //This line keeps the link on current website instead of WordPress.org
}


//* Login Screen: Set 'remember me' to be checked
add_action( 'init', 'pg_login_checked_remember_me' );
function pg_login_checked_remember_me() {

  add_filter( 'login_footer', 'pg_rememberme_checked' )
  ;
}

function pg_rememberme_checked() {

  echo "<script>document.getElementById('rememberme').checked = true;</script>";

}

//* Login Screen: Don't inform user which piece of credential was incorrect
add_filter ( 'login_errors', 'pg_failed_login' );
function pg_failed_login () {

  return 'The login information you have entered is incorrect. Please try again.';

}

//* Modify the admin footer text
add_filter( 'admin_footer_text', 'pg_modify_footer_admin' );
function pg_modify_footer_admin () {

  echo '<span id="footer-thankyou">Theme Development by <a href="http://pinegrow.com" target="_blank">Pinegrow Web Editor</a></span>';

}

//* Add theme info box into WordPress Dashboard
add_action('wp_dashboard_setup', 'pg_add_dashboard_widgets' );
function pg_add_dashboard_widgets() {

  wp_add_dashboard_widget('wp_dashboard_widget', 'Theme Details', 'pg_theme_info');

}


function pg_theme_info() {

  echo "<ul>
  <li><strong>Developed By:</strong> Pinegrow Web Editor</li>
  <li><strong>Website:</strong> <a href='http://pinegrow.com'>pinegrow.com</a></li>
  <li><strong>Contact:</strong> <a href='mailto:support@pinegrow.com'>support@pinegrow.com</a></li>
  </ul>";

}

//* Add Custom Post Types to Tags and Categories in WordPress
//* https://premium.wpmudev.org/blog/add-custom-post-types-to-tags-and-categories-in-wordpress/
//* If you’d like to add only specific post types to listings of tags and categories you can replace the line:
//* $post_types = get_post_types();
//* with
//* $post_types = array( 'post', 'your_custom_type' );
function add_custom_types_to_tax( $query ) {
if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {

//* Get all your post types
$post_types = get_post_types();

$query->set( 'post_type', $post_types );
return $query;
}
}
add_filter( 'pre_get_posts', 'add_custom_types_to_tax' );

//* Remove Jetpack Sharing Buttons to appear in Excerpts
//* https://wordpress.org/support/topic/sharing-icons-show-after-excerpt-and-content
function jptweak_remove_exshare() {
	remove_filter( 'the_excerpt', 'sharing_display',19 );
}
add_action( 'loop_end', 'jptweak_remove_exshare' );

//* Relevanssi Search Shortcode
//* invoke the search form with [search_form].
add_shortcode('search_form', 'rlv_search_form');
function rlv_search_form() {
$url = get_site_url();
$form = <<<EOH
<form role="search" method="get" id="searchform" class="searchform" action="$url">
<label class="screen-reader-text" for="s">Search for:</label>
<input type="text" value="" name="s" id="s" />
<input type="submit" id="searchsubmit" value="Search" />
</form>
EOH;
return $form;
}

//* Jetpack Social menu
//* https://themeshaper.com/2016/02/12/jetpack-social-menu/
//* https://jetpack.com/support/social-menu/
 function jetpackme_social_menu() {
     if ( ! function_exists( 'jetpack_social_menu' ) ) {
         return;
     } else {
         jetpack_social_menu();
     }
 }

 //* Change Jetpack Related Post Headline
//* https://jetpack.com/support/related-posts/customize-related-posts/#headline
function jetpackme_related_posts_headline( $headline ) {
$headline = sprintf(
'<h3 class="jp-relatedposts-headline"><strong>%s</strong></h3>',
esc_html( 'Similar Stuff Going On' )//change your headline here
);
return $headline;
}
add_filter( 'jetpack_relatedposts_filter_headline', 'jetpackme_related_posts_headline' );

//* Remove the Related Posts from your posts
//* https://jetpack.com/support/related-posts/customize-related-posts/
function jetpackme_remove_rp() {
    if ( class_exists( 'Jetpack_RelatedPosts' ) ) {
        $jprp = Jetpack_RelatedPosts::init();
        $callback = array( $jprp, 'filter_add_target_to_dom' );
        remove_filter( 'the_content', $callback, 40 );
    }
}
add_filter( 'wp', 'jetpackme_remove_rp', 20 );




//* MORE MISC USEFUL SNIPPETS //

//* Use Related Posts with Custom Post Types
//* https://jetpack.com/support/related-posts/customize-related-posts/#related-posts-custom-post-types
// function allow_my_post_types($allowed_post_types) {
//     $allowed_post_types[] = 'your-post-type';
//     return $allowed_post_types;
// }
// add_filter( 'rest_api_allowed_post_types', 'allow_my_post_types' );


/* Prevent Page Scroll When Clicking the More Link */

// function remove_more_link_scroll( $link ) {
// 	$link = preg_replace( '|#more-[0-9]+|', '', $link );
// 	return $link;
// }
// add_filter( 'the_content_more_link', 'remove_more_link_scroll' );

/*  Change excerpt length */
/*  By default the excerpt length is set to return 55 words */

// function custom_excerpt_length( $length ) {
// 	return 30;
// }
// add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/* Displaying a "more…" link when using the the_excerpt() */

// function new_excerpt_more($more) {
//        global $post;
// 	return '<br><br><a class="more-link" href="'. get_permalink($post->ID) . '">Read More</a>';
// }
// add_filter('excerpt_more', 'new_excerpt_more');
